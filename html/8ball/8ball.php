<?php
    session_start();

    $answer = 'Ask me a Question';
    $question = '';

    if (isset($_POST['txt_question'])) {
        $question = $_POST['txt_question'];
    }

    $previous_question = '';
    if (isset ($_SESSION['previous_question'])) {
        $previous_question = $_SESSION['previous_question'];
    }


    $responses = [
        'Ask again... later',
        'What is it you really want to know',
        'Ask a different question',
        'Maybe',
        'Reply is hazy',
        'Perhaps',
        'Better not to say',
        'Concentrate and ask a different question',
        'Cannot predict now',
        'Question is too vague',
        'Question is too brief',
        'Question is too long',
        'Question is too direct',
        'You don\'t want to know'//13
    ];

    if (empty($question)) {
        $answer = 'Please ask a question';
    } else if (Substr($question, -1) != '?') {
        $answer = 'The Magic 8 Ball only excepts questions that end with a Question Mark';
    } else if ($previous_question == $question) {
        $answer = 'The Magic 8 Ball has already answered that question, ask another.';
    } else
    {
        $answer = $responses[mt_rand(0, 13)];
        $_SESSION['previous_question'] = $question;
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <title>Magic 8 Ball</title>
</head>
<body>
<header>
    <?php include('../template/header.php') ?>
</header>
<nav>
    <?php include('../template/nav.php') ?>
</nav>
<main>
    <h1>Magic 8 Ball</h1>

    <p>
        <marquee><?= $answer ?></marquee>
    </p>

    <form method="post" action="8ball.php">

        <p>
            <label for="txt_question">What do you wish to have answered</label><br>
            <input type="text" name="txt_question" id="txt_question" value="<?= $question ?>"/>
        </p>

        <input type="submit" value="Hear the Magical 8 Ball's Wisdom" />

    </form>
</main>
<footer>
    <?php include('../template/footer.php') ?>
</footer>
</body>
</html>