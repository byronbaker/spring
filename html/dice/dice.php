<?php
session_start();

$match_number = 0;
if (isset ($_SESSION['match_number'])) {
    $match_number = $_SESSION['match_number'] + 1;
}
$_SESSION['match_number'] = $match_number;

$dice_image = [
    'img//dice_1.png',
    'img//dice_2.png',
    'img//dice_3.png',
    'img//dice_4.png',
    'img//dice_5.png',
    'img//dice_6.png'
];

if($match_number == 0) {
    $paragraph_one = "Get ready for a game of chance.";
    $paragraph_two = "You get two dice the computer gets three.";
    $paragraph_three = "Can you beat the machine?";
    $dice_1=$dice_image[5];
    $dice_2=$dice_image[5];
    $dice_3=$dice_image[0];
    $dice_4=$dice_image[0];
    $dice_5=$dice_image[0];
} else {
    $rand_1=rand(0,5);
    $rand_2=rand(0,5);
    $rand_3=rand(0,5);
    $rand_4=rand(0,5);
    $rand_5=rand(0,5);
    $dice_1=$dice_image[$rand_1];
    $dice_2=$dice_image[$rand_2];
    $dice_3=$dice_image[$rand_3];
    $dice_4=$dice_image[$rand_4];
    $dice_5=$dice_image[$rand_5];
    $paragraph_one = "Your score: ".($rand_1+$rand_2+2);
    $paragraph_two = "Computers score: ".($rand_3+$rand_4+$rand_5+3);
    if ($rand_1+$rand_2 < $rand_3+$rand_4+$rand_5) {
        $_SESSION['loses']++;
        $paragraph_three = "You Lose (".$_SESSION['loses']."-".$_SESSION['wins'].")";
    } else {
        if ($rand_1 + $rand_2 > $rand_3 + $rand_4 + $rand_5) {
            $_SESSION['wins']++;
            $paragraph_three = "You Win (" . $_SESSION['loses'] . "-" . $_SESSION['wins'] . ")";
        } else {
            $paragraph_three = "Tie Game (" . $_SESSION['loses'] . "-" . $_SESSION['wins'] . ")";
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <title>Dice Assignment</title>
</head>
<body>
<header>
    <?php include('../template/header.php') ?>
</header>
<nav>
    <?php include('../template/nav.php') ?>
</nav>
<main>
    <h1>Dice Game</h1>
    <p><?= $paragraph_one ?></p>
    <img src="<?= $dice_1 ?>">
    <img src="<?= $dice_2 ?>">
    <p><?= $paragraph_two  ?></p>
    <img src="<?= $dice_3 ?>">
    <img src="<?= $dice_4 ?>">
    <img src="<?= $dice_5 ?>">
    <p><?= $paragraph_three  ?></p>

    <form method="post" action="dice.php">



        <input type="submit" value="Try your Luck" />

    </form>
</main>
<footer>
    <?php include('../template/footer.php') ?>
</footer>
</body>
</html>