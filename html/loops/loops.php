<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <title>Loops</title>
</head>
<body>
<header>
    <?php include('../template/header.php') ?>
</header>
<nav>
    <?php include('../template/nav.php') ?>
</nav>
<main>
    <h1>Loops and String Fucntions Demo</h1>

    <h3>Basic Vars</h3>
    <?php
        $number = 100;
        print $number;
        echo "<br></strong>".$number."</strong>";

        echo "<br><strong>";
        echo $number;
        echo "</strong>";

        echo "<br><strong>$number</strong>";
        echo '<br><strong>$number</strong>';
        echo "<br><strong>{$number}</strong>";

        $result = "<br><strong>";
        $result .= $number;
        $result .= "</strong>";
    ?>
    <h3>Math</h3>
    <?php
        $number_1 = 100;
        $number_1 = "50";
        $number_1 = 50;

        $result_1 = $number_1 + $number_2;
        $result_2 = $number_1 + $number_3;

        echo "<br>".$result_1;
        echo "<br>".$result_2;
    ?>
    <h3>While Loop</h3>
    <?php
        $i = 1;
        while($i < 7) {
            echo "<h$i>Howdy World!</h$i>";
            $i++;
        }
    ?>
    <h3>Do While Loop</h3>
    <?php
        do{
            echo "<h$i>Howdy World!</h$i>";
            $i--;
        }while($i > 0)
    ?>
    <h3>For Loop</h3>
    <?php
        for($i = 1; $i < 6; $i++) {
            echo "<h$i>Howdy World!</h$i>";
        }
    ?>
    <h3>More Strings</h3>
    <?php
        $full_name = 'Bob Smith';
        $position = strpos($full_name, " ");
        echo "<br> the space position is in position $position";
        echo "<br>".$full_name;
        echo "<br>".strtoupper($full_name);
        echo "<br>".mb_strtolower($full_name);
        $name_parts = explode(" ", $full_name);
        echo "<br>First Name:".$name_parts[0];
        echo "<br>Last Name:".$name_parts[1];


    ?>
</main>
<footer>
    <?php include('../template/footer.php') ?>
</footer>
</body>
</html>