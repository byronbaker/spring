<?php
    /*if (
        isset ($_POST['movie_name']) && !empty($_POST['movie_name'])
        && isset ($_POST['movie_rating']) && !empty($_POST['movie_rating'])
    ) {
        //echo "<pre>"; print_r($POST);  echo "</pre>";

        $title = $_POST['movie_name'];
        $rating = $_POST['movie_rating'];

        include('../template/db_conn.php');

        try {
            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);

            $sql = $db->prepare("INSERT INTO phpclass.movielist(movieTitle, movieRating)
            VALUE (:Title, :Rating)");
            $sql->bindValue(':Title', $title);
            $sql->bindValue(':Rating', $rating);
            $sql->execute();

            header("Location:list.php?success=1");

        } catch (PDOException $e) {
            $error = $e->getMessage();
            echo "Error: $error";
        }
    } else if (isset($_POST) && !empty($_POST)) {
        $error = "Please ensure you have added both a title and rating.";
    }*/
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="base.css">
    <title>Add Move</title>
</head>
<body>
<header>
    <?php include('../template/header.php') ?>
</header>
<nav>

    <?php include('../template/nav.php') ?>
</nav>
<main>
    <h1>Add Movie</h1>

    <form method="post">

        <?php if(isset ($error)) { ?>
            <p class="error"><?= $error; ?></p>
        <?php } ?>

        <table>

            <tr height ="100">
                <th colspan="2">Add New Movie</th>
            </tr>

            <tr height ="50">
                <th>Movie Name</th>
                <td><input type="text" name="movie_name" id="movie_name"/></td>
            </tr>

            <tr height ="50">
                <th>Movie Rating</th>
                <td><input type="text" name="movie_rating" id="movie_rating"/></td>
            </tr>

            <tr height ="100">
                <td colspan="2"><input type="submit"</td>
            </tr>

        </table>
    </form>
</main>
<footer>

    <?php include('../template/footer.php') ?>
</footer>
</body>
</html>