
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="base.css">
    <title>Movie List</title>
</head>
<body>
<header>
    <?php include('../template/header.php') ?>
</header>
<nav>

    <?php include('../template/nav.php') ?>
</nav>
<main>
    <h3>My Movie List</h3>

    <?php if(isset ($_GET['success'])) { ?>
        <p class="success">New Movie Added!</p>
    <?php } ?>

    <table border="1" width="50%">

        <tr>
            <th>Key</th>
            <th>Movie Title</th>
            <th>Rating</th>
        </tr>
        <?php

        include('../template/db_conn.php');

        try {
            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
            $sql = $db->prepare("SELECT * from movielist");
            $sql->execute();
            $row = $sql->fetch();

            while ($row!=null) {

                echo "<tr>";
                echo "<td>" . $row["movieId"] ."</td>";
                echo "<td>" . $row["movieTitle"] ."</td>";
                echo "<td>" . $row["movieRating"] ."</td>";
                echo "<tr>";
                $row = $sql->fetch();
            }

        } catch (PDOException $e) {
            $error = $e->getMessage();
            echo "Error: $error";
            exit;
        }



        ?>


    </table>
    <p>
        <a href="movieadd.php">Add New Movie</a>
    </p>
</main>
<footer>

    <?php include('../template/footer.php') ?>
</footer>
</body>
</html>