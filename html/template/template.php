<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <title>Homepage</title>
</head>
<body>
    <header>
        <?php include('../template/header.php') ?>
</header>
<nav>

    <?php include('../template/nav.php') ?>
</nav>
<main>

</main>
<footer>

    <?php include('../template/footer.php') ?>
</footer>
</body>
</html>