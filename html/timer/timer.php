<?php
    $SEC_PER_MIN = 60;
    $SEC_PER_HOUR = 60 * $SEC_PER_MIN;
    $SEC_PER_DAY = 24 * $SEC_PER_HOUR;

    $NOW = time();
    $END_SEMESTER = mktime(0,0,0,5,20,2022);

    $total_seconds = $END_SEMESTER - $NOW;

    $total_days = floor($total_seconds/$SEC_PER_DAY);

    $total_seconds = $total_seconds - ($SEC_PER_DAY*$total_days);

    $total_hours = floor($total_seconds / $SEC_PER_HOUR);

    $total_seconds = $total_seconds - ($SEC_PER_HOUR*$total_hours);

    $total_minutes = floor($total_seconds / $SEC_PER_MIN);

    $total_seconds = $total_seconds - ($SEC_PER_MIN*$total_minutes);


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <title>Countdown Timer</title>
</head>
<body>
<header>
    <?php include('../template/header.php') ?>
</header>
<nav>
    <?php include('../template/nav.php') ?>
</nav>
<main>
    <h1>Countdown Timer</h1>
    <p><?= date("Y-m-d H:i:s")?></p>
    <p>Now: <?= $NOW ?></p>
    <p>End of  Semester: <?= $END_SEMESTER ?></p>
    <p>Days: <?= $total_days ?> | Hours: <?= $total_hours ?> | Minutes: <?= $total_minutes ?> | Seconds: <?= $total_seconds ?> </p>
</main>
<footer>
    <?php include('../template/footer.php') ?>
</footer>
</body>
</html>